package com.itb.itsmyturn.ui.register;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.itb.itsmyturn.model.RegisterAuth;
import com.itb.itsmyturn.model.RegisterUser;
import com.itb.itsmyturn.repository.Repository;

public class RegisterViewModel extends ViewModel {
    Repository repository = new Repository();
    public LiveData<RegisterAuth> register(RegisterUser registerUser) {
        return repository.register(registerUser.getName(), registerUser.getEmail(), registerUser.getPassword(), registerUser.isTeacher());
    }
    // TODO: Implement the ViewModel
}
