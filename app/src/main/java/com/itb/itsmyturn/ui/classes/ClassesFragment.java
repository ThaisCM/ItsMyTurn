package com.itb.itsmyturn.ui.classes;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.Subject;
import com.itb.itsmyturn.model.SubjectsList;
import com.itb.itsmyturn.utils.Constants;

import static android.content.Context.MODE_PRIVATE;

public class ClassesFragment extends Fragment {

    private ClassesViewModel mViewModel;
    private RecyclerView rvClasses;
    private ClassesAdapter adapter;
    private FloatingActionButton fabAddClass;
    private String email = "";
    private boolean isTeacher;
    private TextView tvPassword;
    private SharedPreferences sp;
    private SharedPreferences.Editor ed;

    public static ClassesFragment newInstance() {
        return new ClassesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.classes_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvClasses = view.findViewById(R.id.rvClasses);
        fabAddClass = view.findViewById(R.id.fabAddClass);
        tvPassword = view.findViewById(R.id.tvClassPasswordText);
        email = ClassesFragmentArgs.fromBundle(getArguments()).getEmailUser();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(ClassesViewModel.class);

        rvClasses.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rvClasses.setLayoutManager(layoutManager);
        adapter = new ClassesAdapter();
        rvClasses.setAdapter(adapter);
        adapter.setOnClassesClickListener(this::onClasseClicked);

        sp = this.getActivity().getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
        isTeacher = sp.getBoolean(Constants.IS_TEACHER, false);

        adapter.setIsTeacher(isTeacher);
        LiveData<SubjectsList> classes;
        if(isTeacher){
            tvPassword.setVisibility(View.VISIBLE);
            classes = mViewModel.getClassesByTeacher(email);
        } else {
            tvPassword.setVisibility(View.GONE);
            classes = mViewModel.getClassesByStudent(email);
        }
        classes.observe(getViewLifecycleOwner(), this::onClasseChanged);

        fabAddClass.setOnClickListener(this::onFABAddClicked);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.miLogOut) {
            logOut();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logOut(){
        ed = sp.edit();
        ed.putString(Constants.KEY_EMAIL, null);
        ed.putBoolean(Constants.IS_LOGGED,false);
        ed.apply();
        Navigation.findNavController(getView()).navigate(R.id.action_classesFragment_to_loginFragment);
    }

    private void onFABAddClicked(View view) {
        if (isTeacher) {
            NavDirections action = ClassesFragmentDirections.actionClassesFragmentToNewClassFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        } else {
            NavDirections action = ClassesFragmentDirections.actionClassesFragmentToAddClassFragment(email);
            Navigation.findNavController(getView()).navigate(action);
        }
    }

    private void onClasseChanged(SubjectsList subjectsList) {
        adapter.setClasses(subjectsList.getSubjectsList());
    }

    private void onClasseClicked(Subject subject) {
        NavDirections action = ClassesFragmentDirections.actionClassesFragmentToTurnFragment(subject.getId(), email);
        Navigation.findNavController(getView()).navigate(action);
    }

}
