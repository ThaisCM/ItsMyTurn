package com.itb.itsmyturn.ui.turn;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.itsmyturn.R;
import com.itb.itsmyturn.model.TurnList;
import com.itb.itsmyturn.model.TurnResponse;
import com.itb.itsmyturn.utils.Constants;

import static android.content.Context.MODE_PRIVATE;

public class TurnFragment extends Fragment {

    private TurnViewModel mViewModel;
    private RecyclerView rvTurns;
    private TurnAdapter adapter;
    private FloatingActionButton fabAddTurn;
    private LiveData<TurnList> turns;
    private String email = "";
    private boolean isTeacher;
    private int idSubject;
    private Handler mRepeatHandler;
    private Runnable mRepeatRunnable;
    private final static int UPDATE_INTERVAL = 120000;
    private SharedPreferences sp;
    SharedPreferences.Editor ed;

    public static TurnFragment newInstance() {
        return new TurnFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRepeatHandler.removeCallbacks(mRepeatRunnable);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.turn_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        rvTurns = view.findViewById(R.id.rvTurns);
        fabAddTurn = view.findViewById(R.id.fabAddTurn);
        idSubject = TurnFragmentArgs.fromBundle(getArguments()).getId();
        email = TurnFragmentArgs.fromBundle(getArguments()).getEmail();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(TurnViewModel.class);

        rvTurns.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false);
        rvTurns.setLayoutManager(layoutManager);
        adapter = new TurnAdapter();
        rvTurns.setAdapter(adapter);
        adapter.setOnTurnClickListener(this::onTurnClicked);

        adapter.setmViewModel(mViewModel);

        sp = this.getActivity().getSharedPreferences(Constants.LOGIN, MODE_PRIVATE);
        isTeacher = sp.getBoolean(Constants.IS_TEACHER, false);
        adapter.setIsTeacher(isTeacher);
        fabAddTurn.setEnabled(!isTeacher);

        turns = mViewModel.getTurnsByClasse(idSubject);
        turns.observe(getViewLifecycleOwner(), this::onTurnChanged);

        fabAddTurn.setOnClickListener(this::onFABAddClicked);
        mRepeatHandler = new Handler();
        mRepeatRunnable = new Runnable() {
            @Override
            public void run() {
                turns = mViewModel.getTurnsByClasse(idSubject);
                turns.observe(getViewLifecycleOwner(), this::onTurnChanged);
                mRepeatHandler.postDelayed(mRepeatRunnable, UPDATE_INTERVAL);
            }

            private void onTurnChanged(TurnList turnList) {
                adapter.setTurns(turnList);
            }


        };
        mRepeatHandler.postDelayed(mRepeatRunnable, UPDATE_INTERVAL);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.miLogOut) {
            logOut();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logOut(){
        ed = sp.edit();
        ed.putString(Constants.KEY_EMAIL, null);
        ed.putBoolean(Constants.IS_LOGGED, false);
        ed.apply();
        Navigation.findNavController(getView()).navigate(R.id.action_turnFragment_to_loginFragment);
    }

    private void onTurnClicked(TurnResponse turnResponse) {
        if(isTeacher) {
            NavDirections action = TurnFragmentDirections.actionTurnFragmentToNewTurnFragment(email, idSubject, true, turnResponse.getId());
            Navigation.findNavController(getView()).navigate(action);
        }
    }

    private void onFABAddClicked(View view) {
        NavDirections action = TurnFragmentDirections.actionTurnFragmentToNewTurnFragment(email, idSubject, false, 0);
        Navigation.findNavController(view).navigate(action);
    }

    private void onTurnChanged(TurnList turnResponses) {
        adapter.setTurns(turnResponses);
    }

}
