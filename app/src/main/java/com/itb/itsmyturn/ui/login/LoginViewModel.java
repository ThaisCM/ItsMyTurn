package com.itb.itsmyturn.ui.login;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.itb.itsmyturn.model.AuthToken;
import com.itb.itsmyturn.model.Login;
import com.itb.itsmyturn.repository.Repository;

public class LoginViewModel extends ViewModel {
    Repository repository = new Repository();

    public LiveData<AuthToken> Login(Login login) {
        return repository.login(login.getEmail(), login.getPassword());
    }

    public LiveData<Boolean> isTeacher(String email){
        return repository.isTeacher(email);
    }
}
