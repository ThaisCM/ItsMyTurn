package com.itb.itsmyturn.ui.classes;

import androidx.lifecycle.ViewModel;

import com.itb.itsmyturn.repository.Repository;

public class NewClassViewModel extends ViewModel {
    Repository repository = new Repository();
    public void newSubject(String name, String group, String email) {
        repository.createClass(name, group, email);
    }
}
