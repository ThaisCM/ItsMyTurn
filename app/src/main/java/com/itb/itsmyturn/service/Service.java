package com.itb.itsmyturn.service;

import com.itb.itsmyturn.model.AuthToken;
import com.itb.itsmyturn.model.RegisterAuth;
import com.itb.itsmyturn.model.SubjectsList;
import com.itb.itsmyturn.model.TurnList;
import com.itb.itsmyturn.model.TurnResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Service {

    @GET("class/turns")
    Call<TurnList> getTurnsByClass(@Query("id") int id);

    @GET("class/teacher")
    Call<SubjectsList> getClassesByTeacher(@Query("email") String email);

    @GET("class/student")
    Call<SubjectsList> getClassesByStudent(@Query("email") String email);

    @GET("login/{email}/{password}")
    Call<AuthToken> login(@Path("email") String email, @Path("password") String password);

    @GET("register")
    Call<RegisterAuth> register(@Query("name") String name, @Query("email") String email, @Query("passwd") String password, @Query("isTeacher") boolean isTeacher);

    @GET("user/isteacher")
    Call<Boolean> isTeacher(@Query("email") String email);

    @GET("user/student")
    Call<String> getNameByEmail(@Query("email") String email);

    @GET("class/newturn")
    Call<String> insertTurn(@Query("email") String email, @Query("idSubject") int idSubject, @Query("time") String time, @Query("description") String description);

    @GET("class/deleteturn")
    Call<String> deleteTurn(@Query("idTurn") int idTurn);

    @GET("class/joinclass")
    Call<String> joinClass(@Query("idSubject") String text, @Query("email")String email);

    @GET("class/newsubject")
    Call<Integer> addClass(@Query(("subjectName")) String name, @Query("idGroup") String group, @Query("email") String email);

    @GET("class/turndescription")
    Call<TurnResponse> getTurnById(@Query("id") int id);

}
