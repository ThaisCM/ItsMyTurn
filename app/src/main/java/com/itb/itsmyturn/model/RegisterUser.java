package com.itb.itsmyturn.model;

import java.io.Serializable;

public class RegisterUser implements Serializable {
    private String name;
    private String email;
    private String password;
    private boolean isTeacher;

    public RegisterUser(String name, String email, String password, boolean isTeacher) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.isTeacher = isTeacher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

}
