package com.itb.itsmyturn.model;

import java.io.Serializable;

public class RegisterAuth implements Serializable {
    private String auth;

    public RegisterAuth(String auth) {
        this.auth = auth;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }
}
