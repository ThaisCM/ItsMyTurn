package com.itb.itsmyturn.model;

import java.io.Serializable;

public class TurnResponse implements Serializable {
    private int id;
    private String student;
    private String time;
    private String description;

    public TurnResponse(int id, String student, String time, String description) {
        this.id = id;
        this.student = student;
        this.time = time;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
