package com.itb.itsmyturn.model;

import java.io.Serializable;
import java.util.List;

public class SubjectsList implements Serializable {
    List<Subject> subjectsList;

    public SubjectsList(List<Subject> subjectsList) {
        this.subjectsList = subjectsList;
    }

    public List<Subject> getSubjectsList() {
        return subjectsList;
    }

    public void setSubjectsList(List<Subject> subjectsList) {
        this.subjectsList = subjectsList;
    }
}
