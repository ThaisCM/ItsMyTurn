package com.itb.itsmyturn.utils;

public class Constants {
    public static String KEY_EMAIL = "Email";
    public static String IS_LOGGED = "IsLogged";
    public static String LOGIN = "Login";
    public static String IS_TEACHER = "IsTeacher";
    public static String NAVIGATE = "Navigate";
}
